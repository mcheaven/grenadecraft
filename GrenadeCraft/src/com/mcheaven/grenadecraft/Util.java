package com.mcheaven.grenadecraft;

import org.bukkit.Location;

import com.massivecraft.factions.entity.BoardColls;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.mcore.ps.PS;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class Util {
	
	public static boolean isWorldGuardProtected(RegionManager mgr, GrenadeCraft plugin, Location loc) {
		if (plugin.wg != null) {
			ApplicableRegionSet rs = mgr.getApplicableRegions(loc);
			if (rs != null && rs.iterator().hasNext())
				return true; //Ist eine WG Region
			else
				return false;
		}
		return false;
	}
	
	public static boolean isInFaction(Location loc, String facName) {
		Faction faction = BoardColls.get().getFactionAt(PS.valueOf(loc));
		if(faction.isNone()) {
			if(facName.equalsIgnoreCase("wilderness"))
				return true;
			else
				return false;
		}
		
		if(faction.getName().equalsIgnoreCase(facName))
			return true;
		return false;
	}

}
