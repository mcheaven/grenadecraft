package com.mcheaven.grenadecraft.grenade;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import com.mcheaven.grenadecraft.GrenadeCraft;
import com.mcheaven.grenadecraft.Util;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class Grenade {

	GrenadeCraft plugin;

	private String name;

	BukkitScheduler scheduler;
	ItemStack is;
	Player player;
	int explosionDelay = 60;
	double power = 10;
	List<String> grenadeType = new ArrayList<String>();
	private int pickupDelay = 120;
	
	private boolean worldguardprotection = false;
	private List<String> protectedfactions = new ArrayList<String>();

	private HEGrenade grenadeHE;
	private FireGrenade grenadeFire;
	private ThunderGrenade grenadeThunder;
	private SmokeGrenade grenadeSmoke;
	private List<PotionGrenade> grenadesPotion = new ArrayList<PotionGrenade>();

	// Constructor:
	public Grenade(String name, GrenadeCraft plugin, Player player, int explosionDelay,
			double power) {
		this.plugin = plugin;
		scheduler = plugin.getServer().getScheduler();
		is = player.getItemInHand();
		this.player = player;
		this.power = power;
		this.explosionDelay = explosionDelay;
		this.name = name;
	}

	private void grenadeLaunch() {
		// Grenade gets thrown in this Method
		Location ploc = player.getEyeLocation();
		ItemStack is2 = new ItemStack(is);
		is2.setAmount(1);
		final Item grenade = player.getWorld().dropItem(ploc, is2);
		int am = is.getAmount();
		if (am > 0)
			is.setAmount(am - 1);
		else
			is = null;
		player.setItemInHand(is);
		grenade.setVelocity(ploc.getDirection());
		grenade.setPickupDelay(pickupDelay);
		final RegionManager mgr = plugin.wg.getRegionManager(player.getWorld());
		scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {

			public void run() {
				Location loc = grenade.getLocation();
				if(isInsideProtection(mgr, loc)) {
					stopExplosion(grenade);
					return;
				}
				grenadeExplode(loc, grenade);
			}
		}, explosionDelay);

	}

	protected void grenadeExplode(Location loc, Item grenade) {
		// Add @Override in child Class to use this Method
		// When adding new Grenade add following:
		long removeTime = 1L;
		if (grenadeHE != null) {
			grenadeHE.setProtections(worldguardprotection, protectedfactions);
			grenadeHE.grenadeExplode(loc, grenade);
		}
		if (grenadeFire != null) {
			grenadeFire.setProtections(worldguardprotection, protectedfactions);
			grenadeFire.grenadeExplode(loc, grenade);
		}
		if (grenadeSmoke != null) {
			grenadeSmoke.setProtections(worldguardprotection, protectedfactions);
			grenadeSmoke.grenadeExplode(loc, grenade);
		}
		if (grenadeThunder != null) {
			grenadeThunder.setProtections(worldguardprotection, protectedfactions);
			grenadeThunder.grenadeExplode(loc, grenade);
		}
		if (!grenadesPotion.isEmpty())
			for (PotionGrenade potionGrenade : grenadesPotion) {
				potionGrenade.setProtections(worldguardprotection, protectedfactions);
				potionGrenade.grenadeExplode(loc, grenade);
				if (potionGrenade.getRemoveTime() > removeTime)
					removeTime = potionGrenade.getRemoveTime();
			}
		removeGrenade(grenade, removeTime);
	}
	
	private void stopExplosion(Item grenade) {
		Location loc = grenade.getLocation();
		loc.getWorld().playEffect(loc, Effect.EXTINGUISH, 20);
		loc.getWorld().playEffect(loc, Effect.SMOKE, BlockFace.UP);
		grenade.setPickupDelay(0);
	}

	//Methode to start the Grenade (Throw)
	public void start() {
		if (!grenadesPotion.isEmpty())
			for (PotionGrenade potionGrenade : grenadesPotion) {
				if (potionGrenade.getRemoveTime() > pickupDelay)
					pickupDelay = potionGrenade.getRemoveTime()
							+ explosionDelay;
			}
		grenadeLaunch();
	}

	//Add an Grenade Object that gets started when Grenade should explode
	public void addGrenade(Grenade grenade) {
		if (grenade == null)
			return;
		// When adding new Grenade add following:
		if (grenade instanceof HEGrenade) {
			grenadeType.add("explosion");
			grenadeHE = (HEGrenade) grenade;
		}
		if (grenade instanceof FireGrenade) {
			grenadeType.add("fire");
			grenadeFire = (FireGrenade) grenade;
		}
		if (grenade instanceof ThunderGrenade) {
			grenadeType.add("thunder");
			grenadeThunder = (ThunderGrenade) grenade;
		}
		if (grenade instanceof SmokeGrenade) {
			grenadeType.add("smoke");
			grenadeSmoke = (SmokeGrenade) grenade;
		}
		if (grenade instanceof PotionGrenade) {
			grenadeType.add("potion");
			grenadesPotion.add((PotionGrenade) grenade);
		}
	}

	private void removeGrenade(final Item grenade, long removeTime) {
		scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				grenade.remove();
			}
		}, removeTime);
	}
	
	public List<String> getGrenadeType() {
		return grenadeType;
	}

	//Get the Grenade Name specified in Config
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setWorldGuardProtection(boolean worldguard) {
		this.worldguardprotection = worldguard;
	}
	
	public void setProtectedFactions(List<String> factions) {
		this.protectedfactions = factions;
	}
	
	public void addProtectedFaction(String faction) {
		protectedfactions.add(faction);
	}
	

	protected boolean isInsideProtection(RegionManager mgr, Location loc) {
		if(worldguardprotection) {
			if(Util.isWorldGuardProtected(mgr, plugin, loc)) {
				return true;
			}
		}
		if(!protectedfactions.isEmpty()) {
			for(String faction : protectedfactions)
				if(Util.isInFaction(loc, faction)) {
					return true;
				}
		}
		return false;
	}
	
	protected void setProtections(boolean worldguard, List<String> factions) {
		worldguardprotection = worldguard;
		protectedfactions = factions;
	}
}
