package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import com.mcheaven.grenadecraft.GrenadeCraft;

public class SmokeGrenade extends Grenade {
	private int blockRange = 1;

	public SmokeGrenade(String name, GrenadeCraft plugin, Player player, int power) {
		super(name, plugin, player, 60, power);
		blockRange = 3;
	}

	@Override
	protected void grenadeExplode(Location loc, Item grenade) {
		createCyl(loc);
		// Need way to add Smoke
	}

	private void createCyl(Location loc) {
		World world = loc.getWorld();
		int cx = loc.getBlockX();
		int cy = loc.getBlockY();
		int cz = loc.getBlockZ();
		int rSquared = blockRange * blockRange;
		long i = 1L;
		long t = 20L;
		for (int y = cy - 1; y <= cy + blockRange; y++) {
			for (int x = cx - blockRange; x <= cx + blockRange; x++)
				for (int z = cz - blockRange; z <= cz + blockRange; z++) {
					if ((cx - x) * (cx - x) + (cz - z) * (cz - z) <= rSquared) {
						Location finLocation = world.getBlockAt(x, y, z).getLocation();
						if (world.getBlockAt(finLocation).isEmpty()) {
							if(random()) {
								continue;
							}
							for(int i2 = 0; i2 <= 10; i2++) {
								Smoke smoke = new Smoke(world, finLocation);
								smoke.setPid(scheduler.scheduleSyncRepeatingTask(plugin, smoke, i+i2*2, t));
							}
							i = i + 3;
							if(i%2 == 0)
								t = 20L;
							else 
								t = 23L;
						}
					}
				}
		}
	}
	
	public static boolean random() {
		int number = (int) (Math.random() * 3 + 2);
		if(number <= 3)
			return false;
		else 
			return true;
	}

}
