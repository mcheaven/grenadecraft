package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import com.mcheaven.grenadecraft.GrenadeCraft;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class FireGrenade extends Grenade {

	private World world;
	private int blockRange = 6;

	public FireGrenade(String name, GrenadeCraft plugin, Player player,
			double range) {
		super(name, plugin, player, 60, range);
		blockRange = (int) range;
	}

	@Override
	protected void grenadeExplode(Location loc, Item grenade) {
		world = loc.getWorld();
		world.playEffect(loc, Effect.POTION_BREAK, 10);
		createCyl(loc);
	}

	private void createCyl(Location loc) {
		int cx = loc.getBlockX();
		int cy = loc.getBlockY();
		int cz = loc.getBlockZ();
		int rSquared = blockRange * blockRange;
		for (int x = cx - blockRange; x <= cx + blockRange; x++) {
			for (int y = cy - blockRange; y <= cy + blockRange; y++)
				for (int z = cz - blockRange; z <= cz + blockRange; z++) {
					if ((cx - x) * (cx - x) + (cz - z) * (cz - z) <= rSquared) {
						setFire(world.getBlockAt(x, y, z));
					}
				}
		}
	}

	private void setFire(Block block) {
		if (!block.isEmpty())
			return;
		RegionManager mgr = plugin.wg.getRegionManager(block.getWorld());

		if (isInsideProtection(mgr, block.getRelative(0, -1, 0).getLocation())
				|| isInsideProtection(mgr, block.getRelative(1, 0, 0)
						.getLocation())
				|| isInsideProtection(mgr, block.getRelative(-1, 0, 0)
						.getLocation())
				|| isInsideProtection(mgr, block.getRelative(0, 0, 1)
						.getLocation())
				|| isInsideProtection(mgr, block.getRelative(0, 0, -1)
						.getLocation()))
			return;

		if (block.getType() == Material.LONG_GRASS)
			block.setType(Material.FIRE);
		if (!block.getRelative(0, -1, 0).isEmpty()) {
			world.playEffect(block.getRelative(0, 1, 0).getLocation(),
					Effect.MOBSPAWNER_FLAMES, 10);
		}
		block.setType(Material.FIRE);
	}

	/*
	 * WIP Broken private boolean blockBetween(Block block1, Block block2) { if
	 * (!block1.isEmpty() || !block2.isEmpty()) return false; int x1 =
	 * block1.getX(); int y1 = block1.getY(); int z1 = block1.getZ();
	 * 
	 * int x2 = block2.getX(); int y2 = block2.getY(); int z2 = block2.getZ();
	 * 
	 * if (x1 > x2) { x1 = x2; x2 = block1.getX(); } if (y1 > y2) { y1 = y2; y2
	 * = block1.getY(); } if (z1 > z2) { z1 = z2; z2 = block1.getZ(); } if (x1
	 * == x2) for (; y1 < y2; y1++) for (; z1 < z2; z1++) if
	 * (!block1.getWorld().getBlockAt(x1, y1, z1).isEmpty()) return true; if (y1
	 * == y2) for (; x1 < x2; x1++) for (; z1 < z2; z1++) if
	 * (!block1.getWorld().getBlockAt(x1, y1, z1).isEmpty()) return true; if(z1
	 * == z2) for (; x1 < x2; x1++) for (; y1 < y2; y1++) if
	 * (!block1.getWorld().getBlockAt(x1, y1, z1).isEmpty()) return true; for (;
	 * x1 < x2; x1++) for (; y1 < y2; y1++) for (; z1 < z2; z1++) if
	 * (!block1.getWorld().getBlockAt(x1, y1, z1).isEmpty()) return true; return
	 * false; }
	 */

}
