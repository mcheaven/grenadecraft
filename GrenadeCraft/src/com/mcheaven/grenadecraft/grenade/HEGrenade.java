package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import com.mcheaven.grenadecraft.GrenadeCraft;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class HEGrenade extends Grenade {
	
	private float explosionPower = 2.0F;
	private int explosionRange = 3;
	private boolean explosionFire = true;
	private boolean destroyBlocks = true;

	public HEGrenade(String name, GrenadeCraft plugin, Player player, double effectPower, boolean fire, boolean destroy) {
		super(name, plugin, player, 60, effectPower);
		explosionPower = (float) effectPower;
		explosionFire = fire;
		destroyBlocks = destroy;
	}
	
	
	
	@Override 
	protected void grenadeExplode(Location loc, Item grenade) {
		explosionRange = Math.round(explosionPower);
		World world = loc.getWorld();
		RegionManager mgr = plugin.wg.getRegionManager(world);
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		for (int x2 = x - explosionRange; x2 <= x + explosionRange; x2++) {
			for (int y2 = y - explosionRange; y2 <= y + explosionRange; y2++)
				for (int z2 = z - explosionRange; z2 <= z + explosionRange; z2++) {
					if(isInsideProtection(mgr, world.getBlockAt(x2, y2, z2).getLocation())) {
						destroyBlocks = false;
						break;
					}
				}
		}
		loc.getWorld().createExplosion(x,y,z, explosionPower, explosionFire, destroyBlocks);
	}

}
