package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;

public class Smoke implements Runnable {
	
	final Location location;
	final Location locationAbove;
	final World world;
	final Effect effect = Effect.SMOKE;
	private int pid;

	Smoke(World world, Location plocation) {
		this.world = world;
		this.location = plocation;
		locationAbove = world.getBlockAt(location.getBlockX(), location.getBlockY()+1, location.getBlockZ()).getLocation();
	}
	
	@Override
	public void run() {
		world.playEffect(locationAbove, effect, BlockFace.UP, 360);
		world.playEffect(location, effect, BlockFace.UP, 360);
		world.playEffect(location, effect, BlockFace.UP, 360);
		world.playEffect(location, effect, BlockFace.EAST, 360);
		world.playEffect(location, effect, BlockFace.NORTH, 360);
		world.playEffect(location, effect, BlockFace.SOUTH, 360);
		world.playEffect(location, effect, BlockFace.WEST, 360);
		
	}
	
	public void setPid(int pid) {
		this.pid = pid;
		System.out.println("Pid set to: "+pid);
	}

}
