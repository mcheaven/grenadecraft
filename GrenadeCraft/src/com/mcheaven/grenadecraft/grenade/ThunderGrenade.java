package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import com.mcheaven.grenadecraft.GrenadeCraft;

public class ThunderGrenade extends Grenade {

	public ThunderGrenade(String name, GrenadeCraft plugin, Player player, double effectPower) {
		super(name, plugin, player, 60, effectPower);
	}
	
	@Override
	protected void grenadeExplode(Location loc, Item grenade) {
		loc.getWorld().strikeLightning(loc);
	}

}
