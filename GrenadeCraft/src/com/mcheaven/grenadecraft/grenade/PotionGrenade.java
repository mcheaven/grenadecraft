package com.mcheaven.grenadecraft.grenade;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.mcheaven.grenadecraft.GrenadeCraft;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class PotionGrenade extends Grenade {
	
	World world;
	PotionEffect effect;
	double power;
	double range;
	int taskId;
	int airTime = 120;

	public PotionGrenade(String name, GrenadeCraft plugin, Player player, PotionEffect effect, double range, int airTime) {
		super(name, plugin, player, 60, range);
		this.effect = effect;
		this.range = range;
		this.airTime = airTime;
	}
	
	@Override
	public void grenadeExplode(Location loc, Item grenade) {
		world = loc.getWorld();
		addEffect(grenade, loc);
		stopAddingEffects();
	}
	
	private void addEffect(final Item grenade, final Location loc) {
		final RegionManager mgr = plugin.wg.getRegionManager(world);
		BukkitTask task = new BukkitRunnable() {
			
			@Override
			public void run() {
				for(Entity entity : grenade.getNearbyEntities(range, range, range)) {
					if(!(entity instanceof LivingEntity))
						continue;
					LivingEntity lentity = (LivingEntity) entity;
					if(lentity.hasPotionEffect(effect.getType()))
						lentity.removePotionEffect(effect.getType());
					Location loc = lentity.getLocation();
					if(!isInsideProtection(mgr, loc)) {
						lentity.addPotionEffect(effect);
						loc.getWorld().playEffect(loc, Effect.MOBSPAWNER_FLAMES, 10);
					}
				}
				world.playEffect(loc, Effect.SMOKE, BlockFace.UP);
			}
		}.runTaskTimer(plugin, 5L, 10L);
		taskId = task.getTaskId();
	}
	
	public void stopAddingEffects() {
		scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				scheduler.cancelTask(taskId);
			}
		}, airTime);
	}
	
	public int getRemoveTime() {
		int removeTime = airTime+10;
		return removeTime;
	}
	

}
