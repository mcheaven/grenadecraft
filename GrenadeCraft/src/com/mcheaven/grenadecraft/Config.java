package com.mcheaven.grenadecraft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.mcheaven.grenadecraft.grenade.FireGrenade;
import com.mcheaven.grenadecraft.grenade.Grenade;
import com.mcheaven.grenadecraft.grenade.HEGrenade;
import com.mcheaven.grenadecraft.grenade.PotionGrenade;
import com.mcheaven.grenadecraft.grenade.ThunderGrenade;

public class Config {

	private GrenadeCraft plugin;
	private Configuration config;
	private Logger logger;

	private boolean checkName = false;
	private boolean sneaking = false;

	private ConfigurationSection grenadesSection;
	private Set<String> grenadeNames;
	private HashMap<Material, Integer> grenadeMaterials = new HashMap<Material, Integer>();

	Config(GrenadeCraft plugin) {
		this.plugin = plugin;
		config = plugin.getConfig();
		logger = plugin.getLogger();
	}

	public void startConfig() {

		String version = plugin.getDescription().getVersion();
		String configversion = config.getString("ConfigVersion", "bla");
		if (!(configversion.equals(version))) {
			config.set("ConfigVersion", version);
			updateConfig();
			logger.log(Level.INFO, "Config updated to " + version);
		}
		loadConfig();
		logger.log(Level.INFO, "Config loaded");
	}

	private void updateConfig() {

		if (!config.contains("CheckItemName"))
			config.set("CheckItemName", false);

		if (!config.contains("OnlyThrowWhileSneaking"))
			config.set("OnlyThrowWhileSneaking", false);
		
		if (!config.contains("Grenades")) {

			List<String> effects = Arrays.asList("explosion:4");
			List<String> description = new ArrayList<String>();
			description.add("&7Explodes like TNT");
			description.add("&4Power: 4");
			config.set("Grenades.Explosive.Material", "EGG");
			config.set("Grenades.Explosive.Durability", 2000);
			config.set("Grenades.Explosive.Description", description.toArray());
			config.set("Grenades.Explosive.Protections", Arrays.asList("WorldGuard","Factions:SafeZone"));
			config.set("Grenades.Explosive.GiveCost", 10);
			config.set("Grenades.Explosive.ThrowCost", 0);
			config.set("Grenades.Explosive.Effects", effects);
			description.clear();
			
			effects = Arrays.asList("thunder:1");
			description.add("&7Creates a Lightning");
			config.set("Grenades.Thunder.Material", "SNOW_BALL");
			config.set("Grenades.Thunder.Durability", 2000);
			config.set("Grenades.Thunder.Description", description.toArray());
			config.set("Grenades.Thunder.Protections", Arrays.asList("WorldGuard","Factions:SafeZone"));
			config.set("Grenades.Thunder.GiveCost", 10);
			config.set("Grenades.Thunder.ThrowCost", 0);
			config.set("Grenades.Thunder.Effects", effects);
			description.clear();
			
			effects = Arrays.asList("fire:5");
			description.add("&7Splashs like a Molotow-Cocktail");
			description.add("&4Range: 5");
			config.set("Grenades.Fireball.Material", "FIREBALL");
			config.set("Grenades.Fireball.Durability", 0);
			config.set("Grenades.Fireball.Description", description.toArray());
			config.set("Grenades.Fireball.Protections", Arrays.asList("WorldGuard","Factions:SafeZone"));
			config.set("Grenades.Fireball.GiveCost", 10);
			config.set("Grenades.Fireball.ThrowCost", 0);
			config.set("Grenades.Fireball.Effects", effects);
			description.clear();
			
			effects = Arrays.asList("potion:2:BLINDNESS:60:1:120");
			description.add("&7Lets near Players become Blind");
			description.add("&4Range: 2");
			config.set("Grenades.Blindness.Material", "COAL");
			config.set("Grenades.Blindness.Durability", 2000);
			config.set("Grenades.Blindness.Description", description.toArray());
			config.set("Grenades.Blindness.Protections", Arrays.asList("WorldGuard","Factions:SafeZone"));
			config.set("Grenades.Blindness.GiveCost", 10);
			config.set("Grenades.Blindness.ThrowCost", 0);
			config.set("Grenades.Blindness.Effects", effects);
			description.clear();
		}

		plugin.saveConfig();
	}

	private void loadConfig() {
		grenadesSection = config.getConfigurationSection("Grenades");
		grenadeNames = grenadesSection.getKeys(true);
		Iterator<String> iterator = grenadeNames.iterator();
		while (iterator.hasNext()) {
			String grenadeName = iterator.next();
			if (grenadeName.contains(".")) {
				iterator.remove();
				continue;
			}
			ConfigurationSection grenadeSection = grenadesSection
					.getConfigurationSection(grenadeName);
			Material material = Material.getMaterial(grenadeSection.getString(
					"Material", "blublub"));
			if (material == null) {
				logger.log(
						Level.WARNING,
						"Config isn't set up correctly! Look at Grenade: "
								+ grenadeName + " Material: "
								+ grenadeSection.getString("Material")
								+ " isn't a correct Material!");
				continue;
			}
			grenadeMaterials.put(material, grenadeSection.getInt("Durability"));
		}

		checkName = config.getBoolean("CheckItemName", false);
	}

	public Grenade getGrenade(ItemStack item, Player player) {
		if (item == null)
			return null;
		Material itemMaterial = item.getType();
		int itemDurability = item.getDurability();
		String itemName = "";
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
			itemName = item.getItemMeta().getDisplayName();
		Grenade grenade = null;

		for (String grenadeName : grenadeNames) {
			ConfigurationSection grenadeSection = grenadesSection
					.getConfigurationSection(grenadeName);
			Material grenadeMaterial = Material.getMaterial(grenadeSection
					.getString("Material"));
			int grenadeDurability = grenadeSection.getInt("Durability", 25565);
			int explosionDelay = grenadeSection.getInt("ExplosionDelay", 60);
			if (grenadeMaterial.equals(itemMaterial)
					&& grenadeDurability == itemDurability) {
				if (checkName && !grenadeName.equalsIgnoreCase(itemName))
					return null;
				double power = 10.0;
				grenade = new Grenade(grenadeName, plugin, player, explosionDelay, power);
				List<String> protections = grenadeSection.getStringList("Protections");
				if(!protections.isEmpty())
					for(String protection : protections) {
						if(protection.equalsIgnoreCase("worldguard")) {
							grenade.setWorldGuardProtection(true);
							if(Util.isWorldGuardProtected(plugin.wg.getRegionManager(player.getWorld()), plugin, player.getLocation())) {
								player.sendMessage(plugin.getPrefix()+"Disallowed here!");
								return null;
							}
						}
						if(protection.contains(":")) {
							List<Integer> borders = getAllBorders(protection, ':');
							String facName = protection.substring(borders.get(1)+1);
							grenade.addProtectedFaction(facName);
							if(Util.isInFaction(player.getLocation(), facName)) {
								player.sendMessage(plugin.getPrefix()+"Disallowed in "+facName);
								return null;
							}
						}
							
					}
				List<String> effects = grenadeSection.getStringList("Effects");
				for (String effect : effects) {
					if (!effect.contains(":") || effect == null) {
						logger.log(Level.WARNING,
								"Config isn't set up correctly! Look at Grenade: "
										+ grenadeName + " Effect: " + effect);
						return null;
					}
					List<String> effectValues = new ArrayList<String>();
					List<Integer> effectValueBorders = getAllBorders(effect,
							':');
					String effectType;
					Double effectPower;
					int i = 1;
					for (int border : effectValueBorders) {
						int border2 = 0;
						try {
							border2 = effectValueBorders.get(i);
						} catch (Exception e) {
							border2 = 0;
						}
						if (border2 == 0)
							effectValues.add(effect.substring(border + 1));
						else if (border == border2)
							effectValues.add(effect.charAt(border + 1) + "");
						else
							effectValues.add(effect.substring(border + 1,
									border2));

						i++;
					}
					effectType = effectValues.get(0);
					try {
						effectPower = Double.valueOf(effectValues.get(1));
					} catch (Exception e) {
						effectPower = 3.0;
					}
					if (effectType.equalsIgnoreCase("fire"))
						grenade.addGrenade(new FireGrenade(grenadeName, plugin, player,
								effectPower));
					if (effectType.equalsIgnoreCase("explosion")) {
						boolean fire = false;
						boolean destroyBlocks = true;
						try {
							fire = Boolean.valueOf(effectValues.get(2));
							destroyBlocks = Boolean
									.valueOf(effectValues.get(3));
						} catch (Exception e) {
							fire = false;
							destroyBlocks = true;
						}
						grenade.addGrenade(new HEGrenade(grenadeName, plugin, player,
								effectPower, fire, destroyBlocks));
					}
					if (effectType.equalsIgnoreCase("thunder")) {
						grenade.addGrenade(new ThunderGrenade(grenadeName, plugin, player,
								effectPower));
					}
					if (effectType.equalsIgnoreCase("potion")) {
						PotionEffect potionEffect = null;
						PotionEffectType type = PotionEffectType.BLINDNESS;
						int duration = 60;
						int amplifier = 1;
						int airTime = 120;
						try {
							type = PotionEffectType.getByName(effectValues
									.get(2));
						} catch (Exception e) {
							type = PotionEffectType.BLINDNESS;
						}
						try {
							duration = Integer.valueOf(effectValues.get(3));
						} catch (Exception e) {
							duration = 60;
						}
						try {
							amplifier = Integer.valueOf(effectValues.get(4));
						} catch (Exception e1) {
							amplifier = 1;
						}
						try {
							airTime = Integer.valueOf(effectValues.get(5));
						} catch (IndexOutOfBoundsException e) {
							airTime = 120;
						}

						potionEffect = new PotionEffect(type, duration,
								amplifier, true);
						grenade.addGrenade(new PotionGrenade(grenadeName, plugin, player,
								potionEffect, effectPower, airTime));
					}
				}
			}
		}

		return grenade;
	}

	public static List<Integer> getAllBorders(String checkString,
			char compareChar) {
		List<Integer> borders = new ArrayList<Integer>();
		borders.add(-1);
		int border = 0;
		for (; border < checkString.length(); border++) {
			if (checkString.charAt(border) == compareChar && border != 0) {
				borders.add(border);
			}
		}
		return borders;
	}

	public Set<String> getGrenadeNames() {
		return grenadeNames;
	}

	public boolean isSneaking() {
		return sneaking;
	}

	public ConfigurationSection getGrenadesSection() {
		return grenadesSection;
	}

	public ConfigurationSection getGrenadeSection(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName != null)
			return grenadesSection.getConfigurationSection(grenadeExactName);
		else
			return null;
	}

	public Material getMaterial(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName != null)
			return Material.getMaterial(grenadesSection
					.getString(grenadeExactName + ".Material"));
		else
			return null;
	}

	public short getDurability(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName != null)
			return Short.valueOf(grenadesSection.getString(grenadeExactName
					+ ".Durability"));
		else
			return 0;
	}
	
	public double getGrenadeThrowCost(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName != null)
			return Double.valueOf(grenadesSection.getString(grenadeExactName
					+ ".ThrowCost", "0.0"));
		else
			return 0;
	}

	public double getGrenadeGiveCost(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName != null)
			return Double.valueOf(grenadesSection.getString(grenadeExactName
					+ ".GiveCost", "0.0"));
		else
			return 0;
	}

	public String getGrenadeExactName(String grenadeName) {
		for (String grenadeExactName : grenadeNames)
			if (grenadeName.equalsIgnoreCase(grenadeExactName))
				return grenadeExactName;
		return null;
	}
	

	//Get the Description of a Grenade with Minecraft Color support using &<color code>
	public List<String> getGrenadeDescription(String grenadeName) {
		String grenadeExactName = getGrenadeExactName(grenadeName);
		if (grenadeExactName == null)
			return null;
		List<String> descriptionOld = grenadesSection.getStringList(grenadeExactName+".Description");
		List<String> descriptionNew = new ArrayList<String>();
		if(descriptionOld == null)
			return null;
		for(String descriptionLine : descriptionOld)
			descriptionNew.add(descriptionLine.replaceAll("(&([a-z0-9]))", "�$2"));
		return descriptionNew;
	}
}
