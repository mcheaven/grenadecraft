package com.mcheaven.grenadecraft.command;

import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.mcheaven.grenadecraft.GrenadeCraft;

public class GiveCommand {

	private GrenadeCraft plugin;
	private String prefix;

	GiveCommand(GrenadeCraft plugin) {
		this.plugin = plugin;
	}

	public boolean execute(String[] args, CommandSender sender, String prefix) {
		this.prefix = prefix;

		if (!(sender instanceof Player) && args.length < 4) {
			sender.sendMessage(prefix
					+ "You must specify a Player when using from Console!");
			return true;
		}

		if (args.length == 1) {
			sendHelpMessage(sender);
			return true;
		}
		if (args[1].equalsIgnoreCase("help")) {
			sendHelpMessage(sender);
			return true;
		}

		String grenadeName = plugin.config.getGrenadeExactName(args[1]);
		if (grenadeName == null) {
			sender.sendMessage(prefix + "Grenade: " + args[1]
					+ " doesn't exist in Config!");
			return true;
		}

		Player player;
		if (args.length < 4)
			player = (Player) sender;
		else {
			player = Bukkit.getPlayer(args[3]);
			if (player == null) {
				sender.sendMessage(prefix + "Player " + args[3]
						+ " isn't online!");
				return true;
			}
		}

		int amount = 1;
		if (args.length > 2)
			try {
				amount = Integer.valueOf(args[2]);
			} catch (NumberFormatException e) {
				sender.sendMessage(prefix + args[2] + " has to be a Number!");
				return true;
			}
		
		Material material = plugin.config.getMaterial(grenadeName);
		if(amount > material.getMaxStackSize())
			amount = material.getMaxStackSize();
		if(!payForGrenade(player, grenadeName, amount))
			return true;
		ItemStack item = new ItemStack(material, amount, plugin.config.getDurability(grenadeName));
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.RESET+grenadeName);
		List<String> description = plugin.config.getGrenadeDescription(grenadeName);
		if(description != null)
			itemMeta.setLore(description);
		item.setItemMeta(itemMeta);
		
		player.getInventory().addItem(item);
		
		return true;

	}

	private void sendHelpMessage(CommandSender sender) {
		sender.sendMessage("-----------------Give-Command-Help-------------------");
		sender.sendMessage(prefix
				+ "/grenade <GrenadeName> [amount] (TargetPlayer)");
		sender.sendMessage(prefix
				+ "If you are the TargetPlayer you can leave that Field empty!");
		sender.sendMessage("-----------------------------------------------------");
	}
	
	private boolean payForGrenade(Player player, String grenade, int grenadeAmount) {
		double cost = plugin.config.getGrenadeGiveCost(grenade) * grenadeAmount;
		if(cost == 0)
			return true;
		
		EconomyResponse econ = plugin.econ.withdrawPlayer(player.getName(), cost);
		if(econ.transactionSuccess()) {
			player.sendMessage(prefix+"Paid "+cost+" to give "+grenade);
			return true;
		} else {
			player.sendMessage(prefix+"Transaction failed!");
			return false;
		}
	}

}
