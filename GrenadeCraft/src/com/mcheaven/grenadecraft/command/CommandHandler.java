package com.mcheaven.grenadecraft.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import com.mcheaven.grenadecraft.GrenadeCraft;

public class CommandHandler implements CommandExecutor {

	private GrenadeCraft plugin;
	private String prefix;

	public CommandHandler(GrenadeCraft plugin, String prefix) {
		this.plugin = plugin;
		this.prefix = prefix;
	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		boolean helpMessage = false;
		
		if(args.length < 1) {
			sender.sendMessage(prefix+plugin.getDescription().getVersion());
			return false;
		}
		
		if(args[0].equalsIgnoreCase("give"))
			helpMessage = new GiveCommand(plugin).execute(args, sender, prefix);
		
		return helpMessage;
	}
}
