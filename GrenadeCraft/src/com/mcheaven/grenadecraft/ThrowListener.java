package com.mcheaven.grenadecraft;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import com.mcheaven.grenadecraft.grenade.Grenade;

public class ThrowListener implements Listener {

	private final GrenadeCraft plugin;
	
	private int throwAmount = 0;
	private double bigCost = 0;

	public ThrowListener(GrenadeCraft instance) {
		plugin = instance;
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void throwGrenade(PlayerInteractEvent event) {
		Action action = event.getAction();
		/*Smoke Test Start
		if ( action == Action.RIGHT_CLICK_AIR) {
			Player playerTest = event.getPlayer();
			Grenade grenadeTest = new Grenade(plugin, playerTest, 60, 1.0);
			grenadeTest.addGrenade(new SmokeGrenade(plugin, playerTest, 5));
			grenadeTest.start();
		}
		*///Smoke Test End
		if (action == Action.LEFT_CLICK_AIR
				|| action == Action.LEFT_CLICK_BLOCK) {
			Player player = event.getPlayer();
			if(plugin.config.isSneaking() && !player.isSneaking())
				return;
			Grenade grenade = plugin.config.getGrenade(player.getItemInHand(), player);
			if(grenade == null)
				return;
			if(!permittedToThrowGrenade(player, grenade))
				return;
			if(!payForGrenade(player, grenade))
				return;
			grenade.start();
		}
	}
	
	private boolean permittedToThrowGrenade(Player player, Grenade grenade) {
		boolean permitted = true;
		
		for(String type : grenade.getGrenadeType()) {
			if(!player.hasPermission("grenadecraft.effect."+type.toLowerCase()))
				permitted = false;
		}
		
		return permitted;
	}
	
	private boolean payForGrenade(Player player, Grenade grenade) {
		String grenadeName = grenade.getName();
		double cost = plugin.config.getGrenadeThrowCost(grenadeName);
		if(cost == 0)
			return true;
		
		EconomyResponse econ = plugin.econ.withdrawPlayer(player, cost);
		if(econ.transactionSuccess()) {
			startPayMessage(player, grenadeName);
			bigCost = bigCost+cost;
			throwAmount++;
			return true;
		} else {
			player.sendMessage(plugin.getPrefix()+"Transaction failed!");
			return false;
		}
	}
	
	private void startPayMessage(final Player player, final String grenadeName) {
		if(throwAmount != 0)
			return;
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				player.sendMessage(plugin.getPrefix()+"Paid "+bigCost+" to throw "+throwAmount+" "+grenadeName+"'s");
				throwAmount = 0;
				bigCost = 0;
			}
		}, 60L);
	}

}
