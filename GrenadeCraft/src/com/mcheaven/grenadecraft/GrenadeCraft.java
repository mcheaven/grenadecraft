package com.mcheaven.grenadecraft;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map.Entry;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.mcheaven.grenadecraft.command.CommandHandler;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class GrenadeCraft extends JavaPlugin {

	public WorldGuardPlugin wg;
	public Economy econ;
	public Config config;
	private String prefix = ChatColor.DARK_RED+"["+ChatColor.GOLD+"GrenadeCraft"+ChatColor.DARK_RED+"] "+ChatColor.RED;
	private long lastPayMessage = 0;
	private Entry<Integer, Double> lastPayment = new AbstractMap.SimpleEntry<Integer, Double>(0, 0.0);

	private final ThrowListener gt = new ThrowListener(this);

	private CommandHandler cmdHandler;

	public void onEnable() {

		config = new Config(this);
		config.startConfig();
		Server server = this.getServer();
		PluginManager pm = server.getPluginManager();
		pm.registerEvents(gt, this);
		wg = (WorldGuardPlugin) pm.getPlugin("WorldGuard");
		cmdHandler = new CommandHandler(this, prefix);
		getCommand("grenade").setExecutor(cmdHandler);
		setupEconomy();
		startMetrics();
	}

	public void onDisable() {

	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer()
				.getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}
	
	private void startMetrics() {
		try {
		    MetricsLite metrics = new MetricsLite(this);
		    metrics.start();
		} catch (IOException e) {
		    // Failed to submit the stats :-(
		}
	}
	
	public String getPrefix() {
		return prefix;
	}

	public long getLastPayMessage() {
		return lastPayMessage;
	}

	public void setLastPayMessage(long lastPayMessage) {
		this.lastPayMessage = lastPayMessage;
	}

	public Entry<Integer, Double> getLastPayment() {
		return lastPayment;
	}

	public void setLastPayment(double newPayment) {
		Entry<Integer, Double> tempPayment;
		if(newPayment == 0)
			tempPayment = new AbstractMap.SimpleEntry<Integer, Double>(0, 0.0);
		else
			tempPayment = new AbstractMap.SimpleEntry<Integer, Double>(lastPayment.getKey()+1, newPayment);
		
		this.lastPayment = tempPayment;
	}
}